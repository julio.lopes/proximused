
'use strict';
/**
 * Provide collapsible parts in html
 */


(function ($) {

    var collapsibleInit = false,
        allopen = false;


    function handleVisibility(elem) {
        var target = $(elem).data('prxcollapse'),
            arrow = $(elem).find('.proxicon-Arrow-down'),
            accordion = $(elem).data('prxcollapse-accordion'),
            addIcon = $(elem).find('.proxicon-Plus'),
            removeIcon = $(elem).find('.proxicon-Collapse');

        // close all other collapsed area's 
        if (accordion) {
            $("[data-prxid]").not("[data-prxid='" + target + "']").hide();
            $('.proxicon-Arrow-down').not(arrow).removeClass('prx-rotated-90');
        }
        $("[data-prxid='" + target + "']").slideToggle('fast');
        $(elem).closest('.prx-panel').toggleClass('prx-open');

        if (arrow.length) {
            arrow.toggleClass('prx-rotated-90');
        }
        if (addIcon.length) {
            //console.log('found it');
            addIcon.removeClass('proxicon-Plus');
            addIcon.addClass('proxicon-Collapse');
        }
        if (removeIcon.length) {
            removeIcon.addClass('proxicon-Plus');
            removeIcon.removeClass('proxicon-Collapse');
        }
    }




    function handleFilterVisibility(ctarget) {

        $($('input:checked')).each(function (index) {
            var target = $(this).data('prxcollapse');
            $("[data-prxid='" + target + "']").fadeIn('fast').removeClass('prx-hidden');

        });

        $($('input:not(:checked)')).each(function (index) {
            var target = $(this).data('prxcollapse');
            $("[data-prxid='" + target + "']").fadeOut('fast');
        });
    }



    $('body').on('click', '[data-prxcollapse]:not("input"):not([data-prxid])', function (e) {
        handleVisibility(this);
    });
    
    $('body').on('click', '[data-prxid]', function (e) {
       e.stopPropagation();
    });




    $('body').on('change', 'input.prx-trigger', function (e) {
        //        console.log(this);
        //        console.log($('input:checked').length);
        if ($('input.prx-trigger:checked').length > 0) {
            if ($('input:checked').length === 1) {
                $("[data-prxid]").hide();
                allopen = false;
                handleFilterVisibility(this);
            } else {

                allopen = false;
                handleFilterVisibility(this);
            }

        } else {
            $("[data-prxid]").show();
            allopen = true;
        }

    });




    function prxBuildCollapsible() {
        if ($j('.prx-collapse-zone.prx-start').length && $j('.prx-collapse-zone.prx-stop')) {


            $j(".prx-collapse-zone.prx-start").nextUntil(".prx-collapse-zone.prx-stop").wrapAll('<div class="prx-wrapper"><div class="prx-collapse-wrapper"></div></div>');


            //hide on the front, display in Rte editor
            $j('.prx-collapse-zone ').css('display', 'none');


            if ($j('.prx-collapse-wrapper').length) {

                $j('.prx-collapse-wrapper  h3:not(:empty)').each(function (index) {
                    var prxid = 'prx-' + index + '-' + Math.floor((Math.random() * 1000) + 1);
                    $j(this).attr('data-prxcollapse', prxid).addClass('prx-trigger');

                    $j(this).nextUntil("h3, main").wrapAll('<section class="prx-collapse__section prx-hidden" data-prxid="' + prxid + '"></section>');
                    if ($j('.prx-collapse-zone.prx-start.addarrow').length) {
                        $j(this).append('&nbsp;&nbsp;<span class="proxicon-Arrow-down"></span>');
                    }

                });

            }
            collapsibleInit = true;
        }
    }

    function initializeVisibility() {
        //replace css display noe by js one, to initialize hidden state
        if ($("[data-prxcollapse]").length) {
            $("[data-prxid].prx-hidden").hide().removeClass('prx-hidden');
        }
    }

    prxBuildCollapsible();
    initializeVisibility();



    // for document viewer tiles, which are loaded with ajax
    $j(document).ajaxComplete(function () {

        if (!collapsibleInit) {
            prxBuildCollapsible();
            initializeVisibility();
        }
    });

}(jQuery));