var is_initialized_css = false,
    is_initialized_mediascreenuser = false;

function addCustomCss() {
    //addCustomCss = function () {}; // kill it as soon as it was called
    if ($j('[data-prx-css]').length) {
        var customCss = $j('[data-prx-css]').attr('data-prx-css');
        $j('head').append("<style>" + customCss + "</style>");

        // hide helper when in document viewer tile, but not when editing
        $j('[data-prx-css]').closest('.j-tileType-content:not(.j-tile-editing)').hide();

        is_initialized_css = true;
    }

}

function addMediaScreenUser() {
    addMediaScreenUser = function () {}; // kill it as soon as it was called
    
    //this the unique identiefier matching the perID of the mediascreen user
    var mediaScreenUser = '995619',
        userId;
    $j.ajax({
        url: "https://proximus-preview.jiveon.com/api/core/v3/people/@me",
        success: function (response) {
            // whatever je wil doen hier als json klaar is met loaden hier
            var profile = response.jive.profile;
            //console.log(profile);
            for (var i = 0; i < profile.length; i++) {

                if (profile[i].jive_label == "MediaUserId") {

                    userId = profile[i].value;
                    console.log(userId)
                }
            }

            if (userId === mediaScreenUser) {
                $j('body').addClass('prx-mediascreen-user');
            }
            is_initialized_mediascreenuser = true;

        }
    });




    // var mediaScreenUser = 'id995619@proximus.com'




}


(function ($j) {

    //make sure it also works in documetn viewer tile, which loads kinda slow
    $j(document).ajaxComplete(function () {
        if (!is_initialized_css) {
            addCustomCss();
        }

        if (!is_initialized_mediascreenuser) {
            addMediaScreenUser()
        }

    });

    //otherwise do it on load
    //    if (!is_initialized_css) {
    //        addCustomCss();
    //    }

    if (!is_initialized_mediascreenuser) {
        addMediaScreenUser()
    }


}(jQuery));