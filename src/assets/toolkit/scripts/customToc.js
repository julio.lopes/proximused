'use strict';
/**
 *  Rewrite out of the box Table of Contents by custom proximus one with sidebar navigation
 */


(function ($j) {

    $j(function () {
       //console.log('prxMainScript');
    //config toc proximus class
    var tocClass = 'prx-toc';
    
    
        //steps tutorials code rewrite to create columns in between tut-limit.stop and tut-limit.start
        function proximusToc() {
            if ($j('.tut-limit.start').length && $j('.tut-limit.stop')) {
    
                if ($j('.weekmenu').length) {
                    $j(".tut-limit.start").nextUntil(".tut-limit.stop").wrapAll('<div class="prx-wrapper weekmenu"><div class="prx-learning-block"></div></div>');
                } else {
                    $j(".tut-limit.start").nextUntil(".tut-limit.stop").wrapAll('<div class="prx-wrapper"><div class="prx-learning-block"></div></div>');
                }
    
                //hide on the front, display in Rte editor
                $j('.tut-limit').css('display', 'none');
                $j('.tut-limit.stop').before('<div class="stickystopper"></div>');
    
                if ($j('.prx-learning-block .toc').length) {
    
                    $j('.prx-learning-block  h2:not(:empty)').each(function () {
                        $j(this).attr('data-magellan-target', ($j(this).attr('id')));
                        $j(this).nextUntil("h2, main").andSelf().wrapAll('<section class="prx-learning-block__section" />');
    
                    });
    
                    //create grid
                    $j('.prx-learning-block .toc').wrap('<div class="prx-small-12 prx-medium-3 prx-columns"></div>');
                    $j('.prx-learning-block .prx-learning-block__section').wrapAll('<div class="prx-small-12 prx-medium-9 prx-columns" />');
                    $j('.prx-learning-block .prx-columns').wrapAll('<div class="prx-row prx-wrapper prx-small-collapse prx-medium-uncollapse"></div>');
    
                    //create sticky table of contents and copy document title on top of toc
                    $j('.toc').addClass(tocClass);
                    // $j('.toc').attr('data-sticky','');
                    var tocHeight = $j('.toc').innerHeight(),
                        viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
                        tocTitle = $j(".j-content-header h1").text();
                    if (viewportWidth > 640) {
                        $j('.' + tocClass).attr('data-magellan', '')
                            .prepend('<div class="prx-toc-title-repeat">' + tocTitle + '</strong>')
                            .scrollToFixed({
                                limit: function () {
                                    var limit = ($j('.stickystopper').offset().top - $j('.toc').innerHeight());
                                    return limit;
                                },
                                minWidth: 640
                            });
                        //console.log($j(this), $j('.' + tocClass).height());
                    }
                }
            }
        }
    
        //check if document viewer tile is present with toc content, and wait for it to finish loading
        if ($j('.j-tileType-content').length) {
            setTimeout(function () {
                proximusToc();
                $j(document).foundation();
            }, 2500);
        } else {
            //do toc script
            proximusToc();
            //initialize foundation
            $j(document).foundation();
        }
    });

}(jQuery));