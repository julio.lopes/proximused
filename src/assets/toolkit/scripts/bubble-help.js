'use strict';
/**
 * Provide help bubbles hooked to certain areas of Jive. Nice for onboarding tracks.Note: never been implemented on production
 */


(function ($j) {

    $j(function () {

        var host = window.location.hostname;
        //console.log(host);
        // do this only on preview and dev
        if (host.indexOf("preview") !== -1 || host.indexOf("localhost") !== -1) {
            // add bubble to certain elements on screen
            var bubbletemplate = '<div class="prx-wrapper"><div class="prx-bubble prx-fadeIn"><div class="prx-bubble__content">Buble text</div><div class="prx-bubble__footer"><div class="prx-bubble__footer__nav js-next-bubble" data-text-fr="Suivante" data-text-nl="Volgende tip">Next-tip <span class="proxicon-Arrow"></span></div><div class="prx-bubble__footer__nav prx-float-right js-close-bubbles" data-text-fr="Fermer" data-text-nl="Sluit tips">Close Tips <span class="proxicon-Close"></span></div></div></div></div>',
                gottemplate = '<div class="prx-wrapper"><div class="prx-central"><button class="prx-button js-next-bubble prx-secondary prx-large">Next tip</button>&nbsp;&nbsp;<button class="prx-button prx-large prx-close-bubbles js-close-bubbles">Close help bubbles</button></div>',

                bubbletext_1 = '<h3>Tip title</h3> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum laudantium ullam dicta suscipit iusto sequi, nam fugit voluptatibus fugiat expedita minima perferendis vel, <a href="#">illo soluta magni ipsum aut</a> soluta magni ipsum aut dolorum dolorem.',
                bubbletext_2 = "<div class='prx-wrapper'>Dit is een bubbletxt 2 <span class='proxicon-Arrow js-next-bubble' title='next'></span></div>",
                bubbletext_3 = "",
                target_1 = $j('#j-nav-search'),
                target_2 = $j('#j-satNav'),
                target_3 = $j('.j-news-menu'),
                step = 1,
                stepMax = 1,
                isBubbleOpen = false;

            var targets = [{
                target: $j('#j-nav-search'),
                bubbletext: '<h3>Tip title</h3> <img src="http://lorempixel.com/500/300/sports/" alt="placeholder image" /> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum laudantium ullam dicta suscipit iusto sequi, nam fugit voluptatibus fugiat expedita minima perferendis vel, <a href="#">illo soluta magni ipsum aut</a> soluta magni ipsum aut dolorum dolorem.'
            },

            {
                target: $j('#j-satNav'),
                bubbletext: "Json Kyle"
            },
            {
                target: $j('.j-news-menu'),
                bubbletext: "<div class='prx-wrapper'>Json Dit is een bubbletxt 3 <span class='proxicon-Arrow js-next-bubble' title='next'></span></div>"
            }

            ];

            stepMax = targets.length;

            // addbubble to body
            $j('body').append(bubbletemplate);

            function stepUpdown(direction) {
                if (direction > 0) {
                    step++;
                } else {
                    step--;
                }
                if (step > stepMax) {
                    step = 1;
                }
                if (step < 1) {
                    step = stepMax;
                }
            }

            var position = 0,
                top = 0,
                left = 0,
                bubbleTop = 0,
                bubbleLeft = 0;

            function getPosition(target) {

                position = target.offset();
                top = position.top;
                left = position.left;
                bubbleTop = top + target.outerHeight();
                bubbleLeft = left;
                //check if bubble is offscreen
                if ((bubbleLeft + 300) > $j('body').width()) {
                    bubbleLeft = bubbleLeft - ((bubbleLeft + 320) - $j('body').width());
                }
                //console.log(bubbleLeft, $j('body').width());
            }

            function blurBgAndPositionBubble(targets) {
                targets.target.addClass('prx-expose');
                getPosition(targets.target);
                $j('.prx-bubble__content').html(targets.bubbletext)
                $j('.prx-bubble').css({
                    'top': bubbleTop,
                    'left': bubbleLeft
                }).addClass('prx-fadeIn');
                var topparent = targets.target.parents("div").last();
                topparent.addClass('expose-parent');
                //hide bell icon 
                $j('#j-satNav-inbox.j-satNav-inbox span.j-navbadge-inbox .j-satNav-inbox-icon').addClass('prx-pseudo-zindex1');
            }

            function cueBubble() {
                //console.log(step);
                cleanBubbles();
                blurBgAndPositionBubble(targets[step - 1]);

            }

            function cleanBubbles() {
                $j('.prx-expose').removeClass('prx-expose');
                $j('.expose-parent').removeClass('expose-parent');
                $j('.prx-blurry').removeClass('prx-blurry');
                $j('.prx-fadeIn').removeClass('prx-fadeIn');
                //put bell icon back again
                $j('#j-satNav-inbox.j-satNav-inbox span.j-navbadge-inbox .j-satNav-inbox-icon').removeClass('prx-pseudo-zindex1');
                // $j('.prx-bubble').remove();

                isBubbleOpen = !isBubbleOpen;
            }

            function toggleBubble() {
                if (!isBubbleOpen) {
                    cueBubble();
                    //console.log('bubble');
                    //$j('body').append(gottemplate);
                    isBubbleOpen = !isBubbleOpen;
                } else {
                    cleanBubbles();
                }
            }

            $j('body').on('click', '.js-bubble-trigger', function () {
                toggleBubble();
                $j('body').addClass('prx-wrapper');
            });

            $j('body').on('click', '.js-next-bubble', function () {
                stepUpdown(1);
                cueBubble();

            });
            $j('body').on('click', '.js-prv-bubble', function () {
                stepUpdown(-1);
                cueBubble();

            });
            $j('body').on('click', '.js-close-bubbles', function () {
                cleanBubbles();
                $j('.prx-bubble').css('left', '-9999px');
                $j('.prx-central').remove();
                isBubbleOpen = false;
            });

            var helptrigger = '<span class="bubble-trigger js-bubble-trigger" style="padding: 4px;color: #da1972;line-height: 0;"><span class="proxicon-Help" style="cursor:pointer;"></span></span>';


            if ($j('#j-links').length) {
                $j('#j-links').append(helptrigger);
                //console.log('bubbles')
            }
        }


    });

}(jQuery));