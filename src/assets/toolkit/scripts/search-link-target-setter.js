// goal: set target on certain urls in search page in jive

function createLinkTarget(target, selector) {
    'use strict';
    // check if we are on the search page
    if ($j('.jive-body-search').length) {
        $j('[data-self^="' + selector + '"]' + ' a').attr("target", target);
    }
}

//all external links get target, internal links not
function createLinkTargetOnAllBut(target, selector) {
    'use strict';
    // check if we are on the search page
    if ($j('.jive-body-search').length) {
        $j('[data-self] a').not(('[data-self^="' + selector + '"]' + ' a')).attr("target", target);
    }
}

(function ($j) {
    'use strict';
    //createLinkTarget('_blank', "https://saml.oase-office.eu");
    var thishost = window.location.protocol + "//" + window.location.host;
    //console.log("thishost: " + thishost);
    createLinkTargetOnAllBut('_blank', thishost);


    // for search results, which are loaded with ajax
    $j(document).ajaxComplete(function () {
        //createLinkTarget('_blank', "https://saml.oase-office.eu");
        createLinkTargetOnAllBut('_blank', thishost);

    });


}(jQuery));