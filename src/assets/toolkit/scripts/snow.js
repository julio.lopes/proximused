// goal: transform a standard 3 column jive lay-out to a custom 1 fullwidth column, followed by an 2/3- 1/3 column layout,ortransform first tile to full width. We do it by including a lay-out helper classes on the page in a unimportant tile

var prxCanvascreated = false,
    prxCanvasTargetSelector,
    prxCanvasTemplate = '<canvas id="prx-canvas" style="position : absolute;top : 0px;background-color: transparent;left : 0px;height: 100%;width: 100%;z-index:-1" width="1160" height="800"></canvas>',
    targetpath = "end-of-year",
    currentpath = window.location.href;

function appendCanvas(selector) {

    $j(selector).append(prxCanvasTemplate);
}


function createSnow() {

    var INTENSITY = 200;

    (function (ns) {
        ns = ns || window;

        function ParticleSystem(ctx, width, height, intensity) {
            this.particles = [];
            intensity = intensity;
            this.addParticle = function () {
                this.particles.push(new Snow(ctx, width));
            }
            while (intensity--) {
                this.addParticle();
            }
            this.render = function () {
                var lingrad = ctx.createLinearGradient(0, 0, 0, 600);
                lingrad.addColorStop(0, '#5d2e91');
                //                lingrad.addColorStop(0.5, '#fff');
                //                lingrad.addColorStop(0.5, '#66CC00');
                lingrad.addColorStop(1, '#ea2f5f');



                ctx.save();
                ctx.fillStyle = lingrad;


                ctx.fillRect(0, 0, width, height);
                for (var i = 0, particle; particle = this.particles[i]; i++) {
                    particle.render();
                }
                ctx.restore();
            }
            this.update = function () {
                for (var i = 0, particle; particle = this.particles[i]; i++) {
                    particle.x += particle.vx;
                    particle.y += particle.vy + 1;
                    if (particle.y > height - 1) {
                        particle.vx = 0;
                        particle.vy = 0;
                        particle.y = height;
                        if (particle.killAt && particle.killAt < +new Date) this.particles.splice(i--, 1);
                        else if (!particle.killAt) {
                            particle.killAt = +new Date + 5000;
                            this.addParticle();
                        }
                    }

                }
            }

        }

        function Snow(ctx, width) {
            this.vx = ((Math.random() - 0.5) * 5);
            this.vy = (Math.random() * 4) + 0.25;
            this.x = Math.floor((Math.random() * width));
            this.y = -Math.random() * 30;
            this.alpha = (Math.random() * 0.99) + 0.15;
            this.radius = Math.random() * 3;
            this.color = 'rgba(255,255,255,1)';
            this.render = function () {
                ctx.globalAlpha = this.alpha;
                ctx.fillStyle = this.color;
                ctx.beginPath();
                ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
                ctx.fill();
            }
        }

        ns.precCanvas = function () {
            var canvas = document.getElementById('prx-canvas');
            var ctx = canvas.getContext('2d');
            var width = canvas.width = 1160;
            var height = canvas.height = 800;
            var particleSystem = new ParticleSystem(ctx, width, height, INTENSITY);
            (function draw() {
                requestAnimationFrame(draw);
                particleSystem.update();
                particleSystem.render();
            })();

        }

    })(window);

    precCanvas();

}

function prxCanvasSetup() {
    
    var fallbacktarget ="body";
    if($j('#j-main-wrapper').length){
        fallbacktarget ='#j-main-wrapper';
    }
    var selector = $j('.prx-createsnow').attr('data-canvastarget') || fallbacktarget ;
    var screenWidth = $j(window).width();

    //only when not mobile
    if (screenWidth > 640) {

        appendCanvas(selector);
        createSnow();
        $j('body').addClass('prx-snow-active');

        if ($j('#j-main').length) {
            $j('.prx-snow-active #j-main').css('padding-top', '20px')
        }

        //console.log('snow snow');
        prxCanvascreated = true;
    }

}


(function ($j) {
    

    $j(document).ajaxComplete(function () {
        if (!prxCanvascreated) {
            if ($j('.prx-createsnow').length) {
                prxCanvasSetup();

            }
        }

    });
    if (!prxCanvascreated) {
        if ($j('.prx-createsnow').length) {
            var selector = $j('.prx-createsnow').attr('data-canvastarget');
            prxCanvasSetup();

        }
    }

}(jQuery));