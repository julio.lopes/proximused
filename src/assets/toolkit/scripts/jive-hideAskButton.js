(function ($j) {

    $j(function () {

        //console.log('it works')
        // ----------------------- ----------------------- ----------------------- //

        //HIDE QUESTION BUTTON IF THERE IS NO QUESTION ASKED
        var handleQuestionBtn = function () {  //POLLING FUNCTION to CHECK IF QUESTION IS EMPTY
            if ($j('.js-ask-a-question-results:empty').length > 0) {
                $j('.j-tileType-question .js-ask-it-button').hide();
            }
            if ($j('.js-ask-a-question-results:empty').length === 0) {
                $j('.j-tileType-question .js-ask-it-button').show();
                clearInterval(handleQuestionBtn);
            }
        };
        //START POLLING
        setInterval(handleQuestionBtn, 100);

        // ----------------------- ----------------------- ----------------------- //
    });

}(jQuery));