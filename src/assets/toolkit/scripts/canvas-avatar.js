// goal: transform a standard 3 column jive lay-out to a custom 1 fullwidth column, followed by an 2/3- 1/3 column layout,ortransform first tile to full width. We do it by including a lay-out helper classes on the page in a unimportant tile

var prxCanvasCreated = false,
    blob, //for IE
    is_IE = false;

//check for Internet Explorer
//if (navigator.userAgent.indexOf("MSIE ") > 0 ||
//    navigator.userAgent.match(/Trident.*rv\:11\./)) {
//    is_IE = true;
//}

if (/MSIE 10/i.test(navigator.userAgent)) {
    // This is internet explorer 10
    is_IE = true;
}

if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
    // This is internet explorer 9 or 11
    is_IE = true;
}

if (/Edge\/\d./i.test(navigator.userAgent)) {
    // This is Microsoft Edge
    is_IE = false;
}

//Global variables
var picWidth = 400; // width of the canvas
var picHeight = 400; // height of the canvas
var picLength = picWidth * picHeight; // number of chunks



function getColorData(myctx, rgbcolor) {

    myImage = myctx.getImageData(0, 0, 400, 400);

    var rgb = rgbcolor;

    rgb = rgb.substring(4, rgb.length - 1)
        .replace(/ /g, '')
        .split(',');

    console.log(Number(rgb[0]));

    var redComponent = Number(rgb[0]),
        greenComponent = Number(rgb[1]),
        blueComponent = Number(rgb[2]);



    // Loop through data.
    for (var i = 0; i < picLength * 4; i += 4) {


        // First bytes are red bytes.        
        // Get red value.
        var myRed = myImage.data[i];

        // Second bytes are green bytes.
        // Get green value.
        var myGreen = myImage.data[i + 1];

        // Third bytes are blue bytes.
        // Get blue value.
        var myBlue = myImage.data[i + 2];

        // Fourth bytes are alpha bytes
        // We don't care about alpha here.
        // Add the three values and divide by three.
        // Make it an integer.
        //myGray = parseInt((myRed + myGreen + myBlue) / 3);
        myGray = parseInt((myRed + myGreen + myBlue) / 3) + 50;

        // Assign average to red, green, and blue.
        //          myImage.data[i] = myGray;
        //          myImage.data[i + 1] = myGray;
        //          myImage.data[i + 2] = myGray;
        myImage.data[i] = redComponent * (myGray / 255);
        myImage.data[i + 1] = greenComponent * (myGray / 255);
        myImage.data[i + 2] = blueComponent * (myGray / 255);
    }
}

function putColorData(myctx) {

    myctx.putImageData(myImage, 0, 0);
}

function prxAvatarCanvasSetup() {

    prxAvatarCanvasSetup = function () {}; // kill it as soon as it was called

    var prxCanvasTemplate = '<canvas class="prx-canvas"></canvas>',
        targetpath = "end-of-year",
        currentpath = window.location.href,
        avatarUrl = $j('.canvas-helper').attr('data-avatar-url');



    function appendCanvas(selector) {
        $j(selector).append(prxCanvasTemplate);
    }

    appendCanvas('.prx-canvas-wrapper');




    var image = new Image(60, 60),
        image2 = new Image(60, 60),
        image3 = new Image(60, 60),
        image4 = new Image(60, 60),
        size = 90,
        colorindex = 0,
        website = '';
    colors = ['#e64894', '#9c84be', 'turquoise', '#8cc85b'];

    if (is_IE) {
        colors = ['rgb(255, 10, 10)', 'rgb(140, 200, 91)', 'rgb(195, 0, 173)', '#rgb(48, 160, 219)'];
    }






    for (var i = 0; i < colors.length; i++) {


    }
    $j(image).on('load', function () {
        drawImageActualSize(image);
    })

    $j(image2).on('load', function () {
        drawImageActualSize(image2);

    });

    $j(image3).on('load', function () {
        drawImageActualSize(image);
    })

    $j(image4).on('load', function () {
        drawImageActualSize(image);
    })


    // load an image
    var d = new Date();
    var n = d.getTime();

    //var imageurl= "https://wap.proximus.com/api/core/v3/people/@me/avatar?width=" + size + '&v=' + n;
    var imageurl = avatarUrl + '?v=' + n;

    image.src = imageurl;
    image.crossorigin = "anonymous";
    image2.src = imageurl;
    image2.crossorigin = "anonymous";
    image3.src = imageurl;
    image3.crossorigin = "anonymous";
    image4.src = imageurl;
    image4.crossorigin = "anonymous";

    //var userName = _jive_current_user.displayName;


    function drawImageActualSize(image) {

        var color = colors[colorindex];


        var canvas = $j('canvas:not(#mycanvas)')[colorindex],
            ctx = canvas.getContext('2d');
        //console.log(color);
        colorindex += 1;

        // use the intrinsic size of image in CSS pixels for the canvas element
        canvas.width = Math.round(image.naturalWidth);
        canvas.height = Math.round(image.naturalHeight);

        if (is_IE) {
            ctx.globalAlpha = 0.75;
            ctx.rect(0, 0, canvas.width, canvas.height);

            ctx.fillStyle = color;
            ctx.fill();

            // ctx.globalCompositeOperation = 'lighter';
            //ctx.filter = 'contrast(100%) brightness(100%) saturate(100%) opacity(100%) grayscale(95%)';
            ctx.globalAlpha = 0.95;


            ctx.drawImage(image, 0, 0);
            // Get and modify the image data.
            getColorData(ctx, color);

            // Put the modified image back on the canvas.
            putColorData(ctx);


        } else {
            ctx.rect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = color;
            ctx.fill();

            ctx.globalCompositeOperation = 'multiply';

            var middle = canvas.width / 2;

            ctx.filter = 'contrast(100%) brightness(125%) saturate(100%) opacity(98%) grayscale(95%)';

            // will draw the image as 300x300 ignoring the custom size of 60x60
            // given in the constructor
            ctx.drawImage(image, 0, 0);



        }


    }



    function convertToDataAndSave() {


        var selected = $j('.prx-selected canvas')[0],
            dataUrl = selected.toDataURL("image/png"),
            random = Math.random(1),
            name = 'santacanvas' + random,
            imageFoo = $j("#download-image"),
            download = $j("#download-link");


        if (is_IE) {
            blob = selected.msToBlob();
            // window.navigator.msSaveBlob(blob, 'Avatar-Strategy-2018.png');
        } else {

            //Create dwonload link
            $j('.js-save a').attr('href', dataUrl);
            $j('.js-save a').attr('download', 'Avatar-Strategy-2018.png');

        }
    };


    //slection logic
    $j('.prx-canvas-wrapper').on("click", function () {
        $j('.prx-canvas-wrapper').removeClass('prx-selected');
        $j(this).addClass('prx-selected');
        convertToDataAndSave();
        if (is_IE) {
            $j('.js-save-ie').show();
        } else {
            $j('.js-save').show();
        }

        $j('.js-user-avatar-upload').show();
        setUploadUrl();
    })
    $j('.js-save').hide();
    $j('.js-save-ie').hide();
    $j('.js-user-avatar-upload').hide();
    $j('.js-user-avatar-upload-inline').hide();
    //undo jive rewriting
    //$('.js-save a').attr('href', '#');

    $j('.js-save a').on("click", function (e) {
        if (is_IE) {
            window.navigator.msSaveOrOpenBlob(blob, 'Avatar-Strategy-2018.png');
            console.log('blob me ');
        }
        $j('.js-user-avatar-upload-inline').show('slow');
    })


    $j('.js-save-ie').on("click", function (e) {

        window.navigator.msSaveOrOpenBlob(blob, 'Avatar-Strategy-2018.png');
        console.log('blob me ie');
        $j('.js-user-avatar-upload-inline').show('slow');
    })




}

// UPload to cloudinary and return image url 
//Cloudinary account needed to make it work.

function prxAjaxUpload() {

    //    Working
    mcanvas = $j('.prx-selected canvas')[0];
    data = mcanvas.toDataURL('image/png');
    var myShinyData = new FormData();
    //you need to specify your unsigned uplaod preset -- create it on cloudinary
    myShinyData.append("upload_preset", "peterpaupreset");
    myShinyData.append("file", data);
    
    
    $j.ajax({
        url: "https://api.cloudinary.com/v1_1/pau/auto/upload",
        type: "POST",
        data: myShinyData,
        processData: false,
        contentType: false,
        success: function (res) {
            console.log(res.secure_url);
            //document.getElementById("response").src = res.secure_url;
            saveAvatarOnWap(res.secure_url)
        }
    });
}

function saveAvatarOnWap(imgUrl) {
    
    var cloud_url = '/api/core/v3/people/@me/avatar/?uri=' + imgUrl ;
    $j.ajax({
        url: cloud_url,
        type: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        success: function (result) {
            // Do something with the result
            console.log( "Image saved to wap");
        }
    });

}


function putAvatarOnWap() {
    
    //    Working
    mcanvas = $j('.prx-selected canvas')[0];
    data = mcanvas.toDataURL('image/png');
    var myShinyData = new FormData();
    
    //you need to specify your unsigned uplaod preset -- create it on cloudinary
    //myShinyData.append("upload_preset", "peterpaupreset");
    myShinyData.append("file", data);
    $j.ajaxSetup({
    headers: {  'Access-Control-Request-Headers': window.parent._jive_auth_token}
    });
    
    
    $j.ajax({
    
        url: "/api/core/v3/people/@me/avatar",
        type: "PUT",
        data: myShinyData,
        processData: false,
        contentType: false,
        success: function (res) {
            console.log(res.secure_url);
        }
    });
    

}



$j('.js-upload').on("click", function (e) {
    prxAjaxUpload();
})



function setUploadUrl() {
    //Create personal avatar upload url
    var userid = _jive_effective_user_id || 111,
        uploadUrl = "/avatar-userupload!inputImage.jspa?targetUser=" + userid;
    if ($j('.js-user-avatar-upload').length) {

        $j('.js-user-avatar-upload a').attr("href", uploadUrl);
    }

    if ($j('.js-user-avatar-upload-inline').length) {

        $j('.js-user-avatar-upload-inline a').attr("href", uploadUrl);
    }
}


(function ($j) {

    $j(document).ajaxComplete(function () {
        if (!prxCanvascreated) {
            if ($j('.canvas-helper').length) {
                prxAvatarCanvasSetup();

            }
        }

    });
    if (!prxCanvascreated) {
        if ($j('.canvas-helper').length) {
            prxAvatarCanvasSetup();

        }
    }

}(jQuery));