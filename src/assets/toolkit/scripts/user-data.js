(function ($) {

    var handleUserPerId = function () {
        $.ajax({
            url: '/api/core/v3/people/@me/?fields=jive',
            context: document.body,
            dataType: 'json',
            dataFilter: function (data, type) {
                var value = data.replace("throw 'allowIllegalResourceCall is false.';", '');
                return value;
            }
        }).done(function (result) {

            $.each(result.jive.externalIdentities, function (profileFieldIndex, profileField) {
                if (profileField.identityType === "LDAP_UUID") {
                    console.log(profileField.identity);

                }
            });
        });

    };

    $('.js-data-perid').on('click', function () {
        handleUserPerId();
        console.log('ok');
    });


})(jQuery);