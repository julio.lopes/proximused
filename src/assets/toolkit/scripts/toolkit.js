'use strict';
/**
 * Toolkit JavaScript
 */

var $j = jQuery.noConflict();

function prxMainScript() {
   
    //insert proximus custom css in tinymce editor
    function addCssToTiny() {

        var head = $j(".mce-edit-area iframe").contents().find("head"),
            prevcss = "https://wapcusto.proximus.com/prev/dist/assets/toolkit/styles/prx-all.css",
            prodcss = "https://wapcusto.proximus.com/dist/assets/toolkit/styles/prx-all.css",
            csspath = '',
            hostname = window.location.hostname;

        csspath = prodcss;
        if (hostname.indexOf("preview") !== -1) {
            csspath = prevcss;
        }

        if (head.length) {
            head.append($j("<link/>", {
                rel: "stylesheet",
                href: csspath,
                type: "text/css"
            }));
        }

    }

    //test ajaxcomplete and load custom css in tiny, do it only once
    var customCssLoaded = 0;
    $j(document).on('ajaxComplete', function (e) {

        if ($j('.mce-tinymce').length && customCssLoaded < 1) {
            console.log('tinyMce active')
            customCssLoaded = 1;
            addCssToTiny();

        }
    });

    // Fill in User name if available:
    var userNameInit = false;

    function renderUserName() {

        if (typeof _jive_current_user != 'undefined' ||  typeof window.top.document._jive_current_user != 'undefined') {
            var user = _jive_current_user ||  window.parent._jive_current_user,
                username = user.displayName,
                firstName = username,
                lastIndex = firstName.lastIndexOf(" "),
                newfirst = username.substr(0, username.indexOf(' ')),
                firstName = newfirst;

            if ($j('.js-prx-nametarget').length) {
                $j('.js-prx-nametarget').text(' ' + firstName.toString().toLowerCase());
                userNameInit = true;
                //console.log('prx rewrittten');
            }
            if ($j('.j-tile:first').hasClass('j-tileType-question')) {
                var orgText = $j('.j-tile:first .j-tile-header h4').text(),
                    newStr = "";
                if (orgText !== "") {
                    newStr = orgText.replace(',', (' ' + firstName.substring(0, 1) + firstName.substring(1, lastIndex).toString().toLowerCase() + ','));
                    $j('.j-tile:first .j-tile-header h4').text(newStr);
                    userNameInit = true;
                    //console.log('asktile rewrittten');
                }

            }

        } else {
            if ($j('.js-prx-nametarget').length) {
                $j('.js-prx-nametarget').text('');
                userNameInit = true;
                //console.log('not rewrittten');
            }

        }

    }
    //do it onlaod 
    renderUserName();


    //or do it on ajax complete
    $j(document).ajaxComplete(function (event, xhr, settings) {

        if (!userNameInit) {
            renderUserName();
        }

    });
    // or after 6secs
    setTimeout(function () {
        if (!userNameInit) {
            renderUserName();
        }
    }, 6000);


} //end prxMainscript



(function ($j) {

    $j(function () {
        prxMainScript();
    });

}(jQuery));