// goal: work around for iframe filtering by jive


function createIframe() {

    if ($j('[data-class="iframe"]').length) {

        $j('[data-class="iframe"]').each(function (index, value) {
            var Murl = $j(this).text();
            //            var decodedUri = decodeURI(Murl);
            console.log(Murl);
            Murl = Murl.replace('<iframe', '');
            Murl = Murl.replace('></iframe>', '');
            Murl = Murl.replace('&lt;iframe', '');
            Murl = Murl.replace('&gt;&lt;/iframe&gt;', '');

            console.log(Murl);
            $j(this).replaceWith('<iframe class="prx-iframe" ' + Murl + '></iframe>');
        });
    } else {
        //console.log('no iframe snippet');
    }
}


(function ($j) {

    createIframe();

}(jQuery));