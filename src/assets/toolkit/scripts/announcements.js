jQuery(document).ready(function () {

    var host = window.location.hostname;
     //console.log(host);
    // do not do it this localhost
    if (host.indexOf("proximus-preview") !== -1 || host.indexOf("wap.proximus") !== -1) {
        $j.getJSON('/api/core/v3/announcements?activeOnly=true', function (data) {
            //create loop to go over all active arrays
            var countAnnouncements = data.list.length;
            for ( var i = 0; i < countAnnouncements; i++) {
                //console.log(countAnnouncements);

                var $html = $j('#announcement-template').clone();
                $j('.subject', $html).html(data.list[i].subject);
                $j('.subject', $html).attr('href', data.list[i].subjectURI);

                $j('.content-text', $html).html(data.list[i].content.text);
                $j('.content-text', $html).attr('href', data.list[i].subjectURI);


                $j('#proximus-announcements').append('<li>' + $html.html() + '</li>');
            }
            //document.getElementById(' announcement-template').style.display='block';
        });
    }




});


jQuery(document).ready(function ($j) {


    setInterval(function () {
        moveRight();
    }, 6000);


    /*varibles null atm*/
    var slideCount = $j('#proximus-announcements li:last-child').length;
    var slideWidth = $j('#proximus-announcements li:last-child').width();
    var slideHeight = $j('#proximus-announcements li:last-child').height();
    var sliderUlWidth = slideCount * slideWidth;
    var testvar = 'hi there';
    //console.log($j('#proximus-announcements li:last-child').width());
    //console.log(slideHeight);
    //console.log(sliderUlWidth);
    //console.log(testvar);

    $j('#proximus-announcements li:last-child').prependTo('#proximus-announcements');

    function moveLeft() {
        $j('#proximus-announcements').animate({
            left: +slideWidth
        }, 200, function () {
            $j('#proximus-announcements li:last-child').prependTo('#proximus-announcements');
            $j('#proximus-announcements').css('left', '');
        });
    };


    function moveRight() {
        $j('#proximus-announcements').animate({
            left: -slideWidth
        }, 200, function () {
            $j('#proximus-announcements li:first-child').appendTo('#proximus-announcements');
            $j('#proximus-announcements').css('left', '');
        });
    };

    $j('a.control_prev').click(function () {
        moveLeft();
    });

    $j('a.control_next').click(function () {
        moveRight();
    });

});