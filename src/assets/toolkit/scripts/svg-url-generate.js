'use strict';
/**
 * When the user clicks on a Svg, he gets the url of the location the svg is hosted at.
 */


(function ($j) {

    $j(function () {


        //    SVG URL GENERATOR

        // sticky url container
        $j('.url-container').scrollToFixed();

        // generate url on clicking svg on dev
        $j('.js-demo-svg img').on('click', function (e) {
            e.preventDefault();
            var url = $j(this).attr('src');
            //console.log(url);
            $j('#svg-icon-url').text(url);
            SelectText('.prx-svg-icon-url');
        });
        // generate url on clicking svg and select url on Jive cause Jive renders a link around images
        $j('.js-demo-svg .prx-svg-demo__item a').on('click', function (e) {
            var url = $j(this).find('img').attr('src');
            //console.log(url);
            e.preventDefault();
            $j('.prx-svg-icon-url').text(url);
            SelectText('.prx-svg-icon-url');
        });

        function SelectText(element) {
            var doc = document,
                text = $j(element)[0],
                range, selection;
            if (doc.body.createTextRange) {
                range = document.body.createTextRange();
                range.moveToElementText(text);
                range.select();
            } else if (window.getSelection) {
                selection = window.getSelection();
                range = document.createRange();
                range.selectNodeContents(text);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }
    });

}(jQuery));