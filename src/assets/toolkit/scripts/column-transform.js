// goal: transform a standard 3 column jive lay-out to a custom 1 fullwidth column, followed by an 2/3- 1/3 column layout,ortransform first tile to full width. We do it by including a lay-out helper classes on the page in a unimportant tile





(function ($j) {
    var isCopyDone = false,
        helpClass3to1 = ".js-columntransformer-3-to-1-2",
        helpClassFirstToFull = ".js-firsttile-fullwidth",
        helpClassFirstOfColumn2ToFull = ".js-firsttile-col2-fullwidth";

    $j(document).ajaxComplete(function () {
        if ($j('.js-columntransformer-3-to-1-2').length) {
            $j('body').addClass('prx-3-to-1-2');
        }
    });



    $j(document).ajaxComplete(function (event, xhr, settings) {

        if (settings.url === "/api/core/v3/executeBatch") {
            //console.log('content tiles fetched');
            //console.log(settings);
            //console.log(xhr.responseText);
            if ($j(helpClass3to1).length) {
                $j('body').addClass('prx-3-to-1-2');
            }
            // do not do it in edit mode
            if ($j('.editMode').length < 1) {
                // first tile will be full width
                if ($j(helpClassFirstToFull).length && !isCopyDone) {
                    //console.log('first tile trigger');
                    $j('body').addClass('prx-firsttile-fullwidth');
                    $j('.j-tile:first').addClass('prx-firsttile-fullwidth prx-medium-hidden');
                    $j('.j-tile:first').clone().prependTo("#datablock-content");
                    $j('#datablock-content .j-tile:first').addClass('prx-fullwidth-firsttile prx-small-hidden').removeClass('prx-medium-hidden');
                    isCopyDone = true;
                }
                // first tile of second column will be full width
                if ($j(helpClassFirstOfColumn2ToFull).length && !isCopyDone) {
                    //console.log('first tile col 2 trigger');
                    setTimeout(function () {

                        $j('body').addClass('prx-firsttile-col2-fullwidth');
                        $j('.js-column-wrap-wide:first .j-tile:first').clone().prependTo("#datablock-content");
                        $j('.js-column-wrap-wide:first .j-tile:first').addClass('prx-firsttile-fullwidth prx-medium-hidden');
                        $j('#datablock-content .j-tile:first').addClass('prx-fullwidth-firsttile prx-small-hidden').css('margin-bottom', '20px');
                        $j('.js-column:first').css('margin-right', '20px');
                        $j('.js-column:first').css('float', 'left');
                        isCopyDone = true;

                    }, 1000);

                }
            }

        }
    });
    //end ajaxcomplete

    window.onload = function () {

        if ($j('.editMode').length < 1) {
            // first tile will be full width
            if ($j(helpClassFirstToFull).length && !isCopyDone) {
                //console.log('first tile trigger');
                setTimeout(function () {
                    $j('body').addClass('prx-firsttile-fullwidth');
                    $j('.j-tile:first').addClass('prx-firsttile-fullwidth prx-medium-hidden');
                    $j('.j-tile:first').clone().prependTo("#datablock-content");
                    $j('#datablock-content .j-tile:first').addClass('prx-fullwidth-firsttile prx-small-hidden').removeClass('prx-medium-hidden');
                    isCopyDone = true;
                    console.log('window load');
                }, 1000);
            }
            // first tile of second column will be full width
            if ($j(helpClassFirstOfColumn2ToFull).length && !isCopyDone) {
                //console.log('first tile col 2 trigger');
                setTimeout(function () {

                    $j('body').addClass('prx-firsttile-col2-fullwidth');
                    $j('.js-column-wrap-wide:first .j-tile:first').clone().prependTo("#datablock-content");
                    $j('.js-column-wrap-wide:first .j-tile:first').addClass('prx-firsttile-fullwidth prx-medium-hidden');
                    $j('#datablock-content .j-tile:first').addClass('prx-fullwidth-firsttile prx-small-hidden').css('margin-bottom', '20px');
                    $j('.js-column:first').css('margin-right', '20px');
                    $j('.js-column:first').css('float', 'left');
                    isCopyDone = true;
                    console.log('window load');

                }, 1000);

            }
        }

    };


}(jQuery));