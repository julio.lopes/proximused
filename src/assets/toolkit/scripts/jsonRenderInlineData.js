/*append gradient border under header menu*/
jQuery(document).ready(function () {
    
    function jsonRenderfromInline() {
        var myvars = [],
            dataUrl = "https://proximus-preview.jiveon.com/docs/DOC-1538-pre-data-json";
        $j(".js-result").load("json-source.html pre.js-json-pre", function () {

            console.log($j('.js-result').text());
            var myJsonObj = JSON.parse($j('.js-result').text());

            //print the output
            for (var i = 0; i < myJsonObj.targets.length; i++) {
                $j(" .template .prx-row").append("<div class='prx-column'><a href='" + (myJsonObj.targets[i].url) + "'>" + (myJsonObj.targets[i].linktext) + "</a></div>");
            }


        });


        var myRows = [],
            headersText = [],
            $headers = $j("th");

        // Loop through grabbing everything
        var $rows = $j(".js-data-table tr").each(function (index) {
            var $cells = $j(this).find("td");
            myRows[index] = {};

            $cells.each(function (cellIndex) {
                // Set the header text
                if (headersText[cellIndex] === undefined) {
                    headersText[cellIndex] = $j($headers[cellIndex]).text().trim().toLowerCase();
                }
                // Update the row object with the header/cell combo
                myRows[index][headersText[cellIndex]] = $j(this).text().trim().toLowerCase();

            });
        });

        // Let's put this in the object like you want and convert to JSON (Note: jQuery will also do this for you on the Ajax request)
        var myObj = {
            "myrows": myRows
        };
        //var myObj = JSON.parse($j(myRows).text());
        console.log(myObj);
        for (var i = 1; i < myObj.myrows.length; i++) {
            $j(" body").append("<h3 class='prx-column'><a href='" + (myObj.myrows[i].url) + "'>" + (myObj.myrows[i].linktext) + "</a></h3");
        }

    }

    jsonRenderfromInline();
});
//console.log("custom.js @wapcusto works");