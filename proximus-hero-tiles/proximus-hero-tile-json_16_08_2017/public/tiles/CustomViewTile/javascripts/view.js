/* global $, gadgets */

var resizetimeout

function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}


$(function () {
  // resize on load
  if (typeof gadgets !== 'undefined') { gadgets.window.adjustHeight() }

  // resize after all images are loaded
  $('img').on('load', function () {
    if (typeof gadgets !== 'undefined') { gadgets.window.adjustHeight() }
  })

  // resize if window is resized
  $(window).resize(function () {
    clearTimeout(resizetimeout)
    //resizetimeout = setTimeout(gadgets.window.adjustHeight, 100)
  })

  $(".proximus-close").on('click', function (e) {
    e.preventDefault();
    $(".onboarding-popup").hide();
    $(".hero-banner").show();
    gadgets.window.adjustHeight()
    createCookie("wappy_will_return","closed", 100);
    return false;
  })

  $('.popup-show').on('click', function (e) {
    e.preventDefault();
    $(".hero-banner").hide();
    $(".onboarding-popup").show();
    //gadgets.window.adjustHeight();
    eraseCookie("wappy_will_return");
    return false;
  })

  // testing, comment for deployment:
  //createCookie("onboarding-1","closed", 100);

if (readCookie("wappy_will_return") != "closed" ) {
  $(".hero-banner").hide();
  $(".onboarding-popup").show();
  //gadgets.window.adjustHeight()
}
  /* Add your JS here */
    
    function jsonRenderfromInline() {
        var myvars = [],
            //dataUrl = "https://proximus-preview.jiveon.com/docs/DOC-1538-pre-data-json";
            dataUrl = "data-source.html";
      $(".js-result").load( dataUrl + " .js-data-table", function () {
          console.log('loading ok');
           // console.log($('.js-result').text());
          
          
          
          var myRows = [],
            headersText = [],
            $headers = $("th");

        // Loop through grabbing everything
        var $rows = $(".js-data-table tr").each(function (index) {
            var $cells = $(this).find("td");
            myRows[index] = {};

            $cells.each(function (cellIndex) {
                // Set the header text
                if (headersText[cellIndex] === undefined) {
                    headersText[cellIndex] = $($headers[cellIndex]).text().trim().toLowerCase();
                }
                // Update the row object with the header/cell combo
                myRows[index][headersText[cellIndex]] = $(this).text().trim().toLowerCase();

            });
        });

        // Let's put this in the object like you want and convert to JSON (Note: jQuery will also do this for you on the Ajax request)
        var myObj = {
            "myrows": myRows
        };
        //var myObj = JSON.parse($(myRows).text());
        console.log(myObj);
//        for (var i = 1; i < myObj.myrows.length; i++) {
//          $(" body").append("<h3 class='prx-column'><a href='" + (myObj.myrows[i].url) + "'>" + (myObj.myrows[i].linktext) + "</a></h3");
//        }
        $("[data-dataId='background-image']").css('background-image', ("url(" + myObj.myrows[1].imageurl + ")") );  
        $("[data-dataId='text-title-fr']").text(myObj.myrows[3].linktext);
        $("[data-dataId='text-title-nl']").text(myObj.myrows[4].linktext);
        $("[data-dataId='text-title-en']").text(myObj.myrows[5].linktext);
        
        $("[data-dataId='text-subtitle-fr']").text(myObj.myrows[6].linktext);
        $("[data-dataId='text-subtitle-nl']").text(myObj.myrows[7].linktext);
        $("[data-dataId='text-subtitle-en']").text(myObj.myrows[8].linktext);
           
        $("[data-dataId='link-fr']").text(myObj.myrows[9].linktext).attr('href', myObj.myrows[9].url);;
        $("[data-dataId='link-nl']").text(myObj.myrows[10].linktext).attr('href', myObj.myrows[10].url);;
        $("[data-dataId='link-en']").text(myObj.myrows[11].linktext).attr('href', myObj.myrows[11].url);;
          
            

        });


        
    }

    jsonRenderfromInline();
    
    
    
    
})

