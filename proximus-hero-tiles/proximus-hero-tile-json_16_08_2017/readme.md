## What's this?

This is a very simple Custom View Tile intended for non-developer use and rapid deployment of simple html tiles - where the standard HTML tile lacks crucial functionality. Primarly translations, api access and responsive design.

## Editing and Installation

Since you're reading this file you already downloaded the zip package and uncompressed it - yay!

The main point of interest is /public/tiles/CustomViewTile/view.html this is the file where you would insert your HTML (potentially inline CSS and JS as well).

You can also customize /public/tiles/CustomViewTile/javascripts/view.js and/public/tiles/CustomViewTile/stylesheets/style.css which are optional. But make sure you don't remove the css and js that's already there.

To create a package you need to compress all the files and directories inside the main directory (definition.json, meta.json, data, i18n and public) into a zip file. Make sure you select all of those, rather then zipping up the top directory - this will cause upload errors.

In your jive instance go to Avatar menu > Addons > "Upload" and upload your newly created zip. The tile will be visible in "External Addons"

When you re-upload the package your tile will be replaced straight away in all instances.

## Multiple packages / tiles

When Creating your own package make sure you update meta.json - the "id" property. You can generate your own UUID in multiple ways - https://www.uuidgenerator.net/ is an easy one.

To add more tiles to your package edit definition.json - you need to add a new object to the "tiles" array. You also need to create a new tile in /public/tiles and match the folder name with the name declared in definition.json.

## Translations

To show different content for different languages use the following syntax:
```
<span lang="en">English text </span>
<span lang="de">Deutscher Text</span>
<span lang="fr">Texte français</span>
```
Those elements will be "display"-ed "inline", if that's not suitable edit the translation section in style.css

## Resizing

The tile will resize (adjust height) on load, images load, and window resize. To fire this event from your script (ie in an api request callback) add:
```
    gadgets.window.adjustHeight()
```
to your script.

## Accessing Jive API

You've got access to the full Jive Javascript API. YAY!
The osapi object is available (docs: https://developers.jivesoftware.com/api/v3/cloud/js/index.html).

## Copyright info

This package is provided by Jive Software - Professional Services. This package is provided as-is without any guarantee or support (unless stated otherwise in an separate agreement). Jive takes no responsibility for any loss or issues resulting from use or misuse of this package.
