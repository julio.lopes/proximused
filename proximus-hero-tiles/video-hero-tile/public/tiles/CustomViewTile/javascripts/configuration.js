/****************************************************
 * This file should load AFTER view.js or container.js, or whichever .js file that defines the onReady, onContainer and onViewer
 *
 * Note:  This implmentation has been provided for convenience, developers are not required to use this pattern.
 *
 * SEE: Tile API & Development FAQ - https://community.jivesoftware.com/docs/DOC-185776
 ****************************************************/

//************************************************************************
//NOTE: CALLED AS SOON AS THE FULL CONTEXT IS RESOLVED
//************************************************************************
function onReady(tileConfig, tileOptions, viewer, container) {

    // make sure config has default value
    if (tileConfig === null) tileConfig = {};
    if (!tileConfig["data"]) {
        tileConfig["data"] = {};
    }
    
//    if (!tileConfig["data"]["configVideoUrl"]) {
//       
//        tileConfig["data"]["configVideoUrl"] = "test_movie.mp4";
//        tileConfig["data"]["configVideoImgFallbackUrl"] = "default url fallback image";
//       
//        tileConfig["data"]["configTitleNl"] = "Title nl";
//        tileConfig["data"]["configTitleFr"] = "Title fr";
//        tileConfig["data"]["configTitleEn"] = "Title en";
//        
//        tileConfig["data"]["configSubTitleNl"] = "SubTitle nl";
//        tileConfig["data"]["configSubTitleFr"] = "SubTitle fr";
//        tileConfig["data"]["configSubTitleEn"] = "SubTitle en";
//    }

    // populate the dialog with existing config value
   
    $("#config_video_url").val(tileConfig["data"]["configVideoUrl"]);
    $("#config_video_img_fallback_url").val(tileConfig["data"]["configVideoImgFallbackUrl"]);
    
    $("#config_title_nl").val(tileConfig["data"]["configTitleNl"]);
    $("#config_title_fr").val(tileConfig["data"]["configTitleFr"]);
    $("#config_title_en").val(tileConfig["data"]["configTitleEn"]);
    
    $("#config_subtitle_nl").val(tileConfig["data"]["configSubTitleNl"]);
    $("#config_subtitle_fr").val(tileConfig["data"]["configSubTitleFr"]);
    $("#config_subtitle_en").val(tileConfig["data"]["configSubTitleEn"]);

    // update config object after clicking submit
    $("#btn_submit").click(function () {
    
        tileConfig["data"]["configVideoUrl"] = $("#config_video_url").val();
        tileConfig["data"]["configVideoImgFallbackUrl"] = $("#config_video_img_fallback_url").val();
        tileConfig["data"]["configTitleNl"] = $("#config_title_nl").val();
        tileConfig["data"]["configTitleFr"] = $("#config_title_fr").val();
        tileConfig["data"]["configTitleEn"] = $("#config_title_en").val();
        tileConfig["data"]["configSubTitleNl"] = $("#config_subtitle_nl").val();
        tileConfig["data"]["configSubTitleFr"] = $("#config_subtitle_fr").val();
        tileConfig["data"]["configSubTitleEn"] = $("#config_subtitle_en").val();
        jive.tile.close(tileConfig, {});
    });

    app.resize();
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE CONFIG IS RESOLVED
//************************************************************************
function onConfig(tileConfig, tileOptions) {
    console.log('onConfig', tileConfig, tileOptions);
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE CONTAINER IS RESOLVED
//************************************************************************
function onContainer(container) {
    console.log('onContainer', container);
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE VIEWER IS RESOLVED
//************************************************************************
function onViewer(viewer) {
    console.log('onViewer', viewer);
} // end function