/****************************************************
 * This file should load BEFORE main.js, since main.js calls the onReady, onContainer and onViewer methods
 * Note:  This implmentation has been provided for convenience, developers are not required to use this pattern.
 *
 * SEE: Tile API & Development FAQ - https://community.jivesoftware.com/docs/DOC-185776
 ****************************************************/

//************************************************************************
//NOTE: CALLED AS SOON AS THE FULL CONTEXT IS RESOLVED
//************************************************************************
function onReady(tileConfig, tileOptions, viewer, container) {
    var screenWidth = $(window).width();

    if (typeof tileConfig !== 'object') {
        tileConfig = JSON.parse(tileConfig || {});
    }

    // Update field with config data

    //video only when not mobile
    if (screenWidth > 640) {

        //we need to set both src and source for IE and dynamic playback
        $("#config_video").attr('src', tileConfig["configVideoUrl"]);
        $("#config_video source").attr('src', tileConfig["configVideoUrl"]);
        //set poster image and falback
        $("#config_video").attr('poster', tileConfig["configVideoImgFallbackUrl"]);
        $("#config_video_image_fallback").attr('src', tileConfig["configVideoImgFallbackUrl"]);
    }


    $("#config_title_nl").html(tileConfig["configTitleNl"]);
    $("#config_title_fr").html(tileConfig["configTitleFr"]);
    $("#config_title_en").html(tileConfig["configTitleEn"]);
    $("#config_subtitle_nl").html(tileConfig["configSubTitleNl"]);
    $("#config_subtitle_fr").html(tileConfig["configSubTitleFr"]);
    $("#config_subtitle_en").html(tileConfig["configSubTitleEn"]);




    // Resize window
    app.resize();
    if (typeof gadgets !== 'undefined') {
        gadgets.window.adjustHeight()
    }
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE CONFIG IS RESOLVED
//************************************************************************
function onConfig(tileConfig, tileOptions) {
    console.log('onConfig', tileConfig, tileOptions);
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE CONTAINER IS RESOLVED
//************************************************************************
function onContainer(container) {
    console.log('onContainer', container);
    if (typeof gadgets !== 'undefined') {
        gadgets.window.adjustHeight()
    }
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE VIEWER IS RESOLVED
//************************************************************************
function onViewer(viewer) {
    console.log('onViewer', viewer);
} // end function



/* global $, gadgets */

var resizetimeout;

function createCookie(name, value, days) {
    //    var expires = "";
    //    if (days) {
    //        var date = new Date();
    //        date.setTime(date.getTime() + (100 * 24 * 60 * 60 * 1000));
    //        expires = "; expires=" + date.toUTCString();
    //    }
    //    document.cookie = name + "=" + value + expires + "; path=/";

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";

    document.cookie = name + "=" + value + "; expires=Thu, 18 Dec 3013 12:00:00 UTC; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    //return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function cookieExists(cname) {
    return (document.cookie.indexOf(cname + '=') == -1) ? false : true;
}


$(function () {
    //change this value every time you create a new version
    var cookieName = "prx-autumn-2017-hero";
    // resize on load
    if (typeof gadgets !== 'undefined') {
        gadgets.window.adjustHeight()
    }

    // resize after all images are loaded
    $('img').on('load', function () {
        if (typeof gadgets !== 'undefined') {
            gadgets.window.adjustHeight()
        }
    })

    // resize if window is resized
    $(window).resize(function () {
        clearTimeout(resizetimeout)
        resizetimeout = setTimeout(function () {
            gadgets.window.adjustHeight();
            gadgets.window.adjustWidth();
        }, 100)
    })

    $(".proximus-close").on('click', function (e) {
        e.preventDefault();
        $(".onboarding-popup").hide();
        $(".hero-banner").show();
        $(".ctas").show();
        gadgets.window.adjustHeight()
        createCookie(cookieName, "closed", 100);
        $('#config_video').load();
        return false;
    })

    $('.popup-show').on('click', function (e) {
        e.preventDefault();
        $(".hero-banner").hide();
        $(".ctas").hide();
        $(".onboarding-popup").show();
        gadgets.window.adjustHeight();
        eraseCookie(cookieName);
        return false;
    })

    // testing, comment for deployment:
    //createCookie(cookieName,"closed", 100);
    var cc = readCookie(cookieName);
    if (cc != "closed") {
        $(".hero-banner").hide();
        $(".ctas").hide();
        $(".onboarding-popup").show();
        //gadgets.window.adjustHeight()
    }
    /* Add your JS here */
})