/****************************************************
 * This file should load AFTER view.js or container.js, or whichever .js file that defines the onReady, onContainer and onViewer
 *
 * Note:  This implmentation has been provided for convenience, developers are not required to use this pattern.
 *
 * SEE: Tile API & Development FAQ - https://community.jivesoftware.com/docs/DOC-185776
 ****************************************************/

//************************************************************************
//NOTE: CALLED AS SOON AS THE FULL CONTEXT IS RESOLVED
//************************************************************************
function onReady(tileConfig, tileOptions, viewer, container) {

    // make sure config has default value
    if (tileConfig === null) tileConfig = {};
    if (!tileConfig["data"]) {
        tileConfig["data"] = {};
    }

    //    if (!tileConfig["data"]["configVideoUrl"]) {
    //       
    //        tileConfig["data"]["configVideoUrl"] = "test_movie.mp4";
    //        tileConfig["data"]["configVideoImgFallbackUrl"] = "default url fallback image";
    //       
    //        tileConfig["data"]["configTitleNl"] = "Title nl";
    //        tileConfig["data"]["configTitleFr"] = "Title fr";
    //        tileConfig["data"]["configTitleEn"] = "Title en";
    //        
    //        tileConfig["data"]["configSubTitleNl"] = "SubTitle nl";
    //        tileConfig["data"]["configSubTitleFr"] = "SubTitle fr";
    //        tileConfig["data"]["configSubTitleEn"] = "SubTitle en";
    //    }

    // populate the dialog with existing config value



    $("#config_video_url").val(tileConfig["data"]["configVideoUrl"]);
    $("#config_video_img_fallback_url").val(tileConfig["data"]["configVideoImgFallbackUrl"]);

    $("#config_title_nl").val(tileConfig["data"]["configTitleNl"]);
    $("#config_title_fr").val(tileConfig["data"]["configTitleFr"]);
    $("#config_title_en").val(tileConfig["data"]["configTitleEn"]);

    $("#config_subtitle_nl").val(tileConfig["data"]["configSubTitleNl"]);
    $("#config_subtitle_fr").val(tileConfig["data"]["configSubTitleFr"]);
    $("#config_subtitle_en").val(tileConfig["data"]["configSubTitleEn"]);

    //Bottom ctas


    $("#bottom_link_1_icon_class").val(tileConfig["data"]["bottomLink1IconClass"]);
    $("#bottom_link_1_text_nl").val(tileConfig["data"]["bottomLink1TextNl"]);
    $("#bottom_link_1_text_fr").val(tileConfig["data"]["bottomLink1TextFr"]);
    $("#bottom_link_1_text_en").val(tileConfig["data"]["bottomLink1TextEn"]);
    $("#bottom_link_1_url_nl").val(tileConfig["data"]["bottomLink1UrlNl"]);
    $("#bottom_link_1_url_fr").val(tileConfig["data"]["bottomLink1UrlFr"]);
    $("#bottom_link_1_url_en").val(tileConfig["data"]["bottomLink1UrlEn"]);

    $("#bottom_link_2_icon_class").val(tileConfig["data"]["bottomLink2IconClass"]);
    $("#bottom_link_2_text_nl").val(tileConfig["data"]["bottomLink2TextNl"]);
    $("#bottom_link_2_text_fr").val(tileConfig["data"]["bottomLink2TextFr"]);
    $("#bottom_link_2_text_en").val(tileConfig["data"]["bottomLink2TextEn"]);
    $("#bottom_link_2_url_nl").val(tileConfig["data"]["bottomLink2UrlNl"]);
    $("#bottom_link_2_url_fr").val(tileConfig["data"]["bottomLink2UrlFr"]);
    $("#bottom_link_2_url_en").val(tileConfig["data"]["bottomLink2UrlEn"]);

    $("#bottom_link_3_icon_class").val(tileConfig["data"]["bottomLink3IconClass"]);
    $("#bottom_link_3_text_nl").val(tileConfig["data"]["bottomLink3TextNl"]);
    $("#bottom_link_3_text_fr").val(tileConfig["data"]["bottomLink3TextFr"]);
    $("#bottom_link_3_text_en").val(tileConfig["data"]["bottomLink3TextEn"]);
    $("#bottom_link_3_url_nl").val(tileConfig["data"]["bottomLink3UrlNl"]);
    $("#bottom_link_3_url_fr").val(tileConfig["data"]["bottomLink3UrlFr"]);
    $("#bottom_link_3_url_en").val(tileConfig["data"]["bottomLink3UrlEn"]);

    $("#bottom_link_4_icon_class").val(tileConfig["data"]["bottomLink4IconClass"]);
    $("#bottom_link_4_text_nl").val(tileConfig["data"]["bottomLink4TextNl"]);
    $("#bottom_link_4_text_fr").val(tileConfig["data"]["bottomLink4TextFr"]);
    $("#bottom_link_4_text_en").val(tileConfig["data"]["bottomLink4TextEn"]);
    $("#bottom_link_4_url_nl").val(tileConfig["data"]["bottomLink4UrlNl"]);
    $("#bottom_link_4_url_fr").val(tileConfig["data"]["bottomLink4UrlFr"]);
    $("#bottom_link_4_url_en").val(tileConfig["data"]["bottomLink4UrlEn"]);


    $("#popover_status").prop('checked', tileConfig["data"]["popoverStatus"]);
    $("#popover_background_image").val(tileConfig["data"]["popoverBackgroundImage"]);
    $("#popover_margin_top").val( tileConfig["data"]["popoverMarginTop"]);

    $("#popover_title_nl").val(tileConfig["data"]["popoverTitleNl"]);
    $("#popover_title_fr").val(tileConfig["data"]["popoverTitleFr"]);
    $("#popover_title_en").val(tileConfig["data"]["popoverTitleEn"]);


    $("#popover_subtitle_nl").val(tileConfig["data"]["popoverSubTitleNl"]);
    $("#popover_subtitle_fr").val(tileConfig["data"]["popoverSubTitleFr"]);
    $("#popover_subtitle_en").val(tileConfig["data"]["popoverSubTitleEn"]);

    //Cta link
    $("#popover_link_text_nl").val(tileConfig["data"]["popoverLinkTextNl"]);
    $("#popover_link_text_fr").val(tileConfig["data"]["popoverLinkTextFr"]);
    $("#popover_link_text_en").val(tileConfig["data"]["popoverLinkTextEn"]);



    $("#popover_link_url_nl").val(tileConfig["data"]["popoverLinkUrlNl"]);
    $("#popover_link_url_fr").val(tileConfig["data"]["popoverLinkUrlFr"]);
    $("#popover_link_url_en").val(tileConfig["data"]["popoverLinkUrlEn"]);


    // laocal stroage var name
    $("#data_storage_name").val(tileConfig["data"]["dataStorageName"]);
    
    $("#script_block").val(tileConfig["data"]["scriptBlock"]);





    // update config object after clicking submit
    $("#btn_submit").click(function () {

        tileConfig["data"]["configVideoUrl"] = $("#config_video_url").val();
        tileConfig["data"]["configVideoImgFallbackUrl"] = $("#config_video_img_fallback_url").val();
        tileConfig["data"]["configTitleNl"] = $("#config_title_nl").val();
        tileConfig["data"]["configTitleFr"] = $("#config_title_fr").val();
        tileConfig["data"]["configTitleEn"] = $("#config_title_en").val();
        tileConfig["data"]["configSubTitleNl"] = $("#config_subtitle_nl").val();
        tileConfig["data"]["configSubTitleFr"] = $("#config_subtitle_fr").val();
        tileConfig["data"]["configSubTitleEn"] = $("#config_subtitle_en").val();

        tileConfig["data"]["bottomLink1IconClass"] = $("#bottom_link_1_icon_class").val();
        tileConfig["data"]["bottomLink1TextNl"] = $("#bottom_link_1_text_nl").val();
        tileConfig["data"]["bottomLink1TextFr"] = $("#bottom_link_1_text_fr").val();
        tileConfig["data"]["bottomLink1TextEn"] = $("#bottom_link_1_text_en").val();
        tileConfig["data"]["bottomLink1UrlNl"] = $("#bottom_link_1_url_nl").val();
        tileConfig["data"]["bottomLink1UrlFr"] = $("#bottom_link_1_url_fr").val();
        tileConfig["data"]["bottomLink1UrlEn"] = $("#bottom_link_1_url_en").val();
        
        tileConfig["data"]["bottomLink2IconClass"] = $("#bottom_link_2_icon_class").val();
        tileConfig["data"]["bottomLink2TextNl"] = $("#bottom_link_2_text_nl").val();
        tileConfig["data"]["bottomLink2TextFr"] = $("#bottom_link_2_text_fr").val();
        tileConfig["data"]["bottomLink2TextEn"] = $("#bottom_link_2_text_en").val();
        tileConfig["data"]["bottomLink2UrlNl"] = $("#bottom_link_2_url_nl").val();
        tileConfig["data"]["bottomLink2UrlFr"] = $("#bottom_link_2_url_fr").val();
        tileConfig["data"]["bottomLink2UrlEn"] = $("#bottom_link_2_url_en").val();
        
        tileConfig["data"]["bottomLink3IconClass"] = $("#bottom_link_3_icon_class").val();
        tileConfig["data"]["bottomLink3TextNl"] = $("#bottom_link_3_text_nl").val();
        tileConfig["data"]["bottomLink3TextFr"] = $("#bottom_link_3_text_fr").val();
        tileConfig["data"]["bottomLink3TextEn"] = $("#bottom_link_3_text_en").val();
        tileConfig["data"]["bottomLink3UrlNl"] = $("#bottom_link_3_url_nl").val();
        tileConfig["data"]["bottomLink3UrlFr"] = $("#bottom_link_3_url_fr").val();
        tileConfig["data"]["bottomLink3UrlEn"] = $("#bottom_link_3_url_en").val();
        
        tileConfig["data"]["bottomLink4IconClass"] = $("#bottom_link_4_icon_class").val();
        tileConfig["data"]["bottomLink4TextNl"] = $("#bottom_link_4_text_nl").val();
        tileConfig["data"]["bottomLink4TextFr"] = $("#bottom_link_4_text_fr").val();
        tileConfig["data"]["bottomLink4TextEn"] = $("#bottom_link_4_text_en").val();
        tileConfig["data"]["bottomLink4UrlNl"] = $("#bottom_link_4_url_nl").val();
        tileConfig["data"]["bottomLink4UrlFr"] = $("#bottom_link_4_url_fr").val();
        tileConfig["data"]["bottomLink4UrlEn"] = $("#bottom_link_4_url_en").val();





        tileConfig["data"]["popoverStatus"] = document.getElementById('popover_status').checked;
        tileConfig["data"]["popoverBackgroundImage"] = $("#popover_background_image").val();
        tileConfig["data"]["popoverMarginTop"] = $("#popover_margin_top").val();

        tileConfig["data"]["popoverTitleNl"] = $("#popover_title_nl").val();
        tileConfig["data"]["popoverTitleFr"] = $("#popover_title_fr").val();
        tileConfig["data"]["popoverTitleEn"] = $("#popover_title_en").val();
        tileConfig["data"]["popoverSubTitleNl"] = $("#popover_subtitle_nl").val();
        tileConfig["data"]["popoverSubTitleFr"] = $("#popover_subtitle_fr").val();
        tileConfig["data"]["popoverSubTitleEn"] = $("#popover_subtitle_en").val();

        tileConfig["data"]["popoverLinkTextNl"] = $("#popover_link_text_nl").val();
        tileConfig["data"]["popoverLinkTextFr"] = $("#popover_link_text_fr").val();
        tileConfig["data"]["popoverLinkTextEn"] = $("#popover_link_text_en").val();
        tileConfig["data"]["popoverLinkUrlNl"] = $("#popover_link_url_nl").val();
        tileConfig["data"]["popoverLinkUrlFr"] = $("#popover_link_url_fr").val();
        tileConfig["data"]["popoverLinkUrlEn"] = $("#popover_link_url_en").val();
        tileConfig["data"]["dataStorageName"] = $("#data_storage_name").val();
        
        tileConfig["data"]["scriptBlock"] = $("#script_block").val();


        jive.tile.close(tileConfig, {});
    });
    collapse();
    app.resize();
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE CONFIG IS RESOLVED
//************************************************************************
function onConfig(tileConfig, tileOptions) {
    console.log('onConfig', tileConfig, tileOptions);
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE CONTAINER IS RESOLVED
//************************************************************************
function onContainer(container) {
    console.log('onContainer', container);
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE VIEWER IS RESOLVED
//************************************************************************
function onViewer(viewer) {
    console.log('onViewer', viewer);
} // end function


function collapse() {
    $(".collapsible-block p").hide();
}

$(function () {
    collapse();

    $(".collapsible-block h3").on('click', function (e) {
        $(this).parent().find('p').toggle("fast", function () {
            // Animation complete.
            if (typeof gadgets !== 'undefined') {
                gadgets.window.adjustHeight();
            }

        });
        $(this).parent().toggleClass('open');

    })



    // testing, comment for deployment:
    //createCookie(cookieName,"closed", 100);


    /* Add your JS here */
})