/****************************************************
 * This file should load BEFORE main.js, since main.js calls the onReady, onContainer and onViewer methods
 * Note:  This implmentation has been provided for convenience, developers are not required to use this pattern.
 *
 * SEE: Tile API & Development FAQ - https://community.jivesoftware.com/docs/DOC-185776
 ****************************************************/

//************************************************************************
//NOTE: CALLED AS SOON AS THE FULL CONTEXT IS RESOLVED
//************************************************************************
var showPopup,
    storageVar;

function onReady(tileConfig, tileOptions, viewer, container) {
    var screenWidth = $(window).width();

    if (typeof tileConfig !== 'object') {
        tileConfig = JSON.parse(tileConfig || {});
    }

    // Update fields with config data

    //video only when not mobile
    if (screenWidth > 640) {

        //we need to set both src and source for IE and dynamic playback
        // $("#config_video").attr('src', tileConfig.configVideoUrl);
        $("#config_video source").attr('src', tileConfig["configVideoUrl"]);
        $('#config_video')[0].load();
        //set poster image and falback
        $("#config_video").attr('poster', tileConfig["configVideoImgFallbackUrl"]);
        $("#config_video_image_fallback").attr('src', tileConfig["configVideoImgFallbackUrl"]);
    }

    //Titles, Subtitles
    $("#config_title_nl").html(tileConfig["configTitleNl"]);
    $("#config_title_fr").html(tileConfig["configTitleFr"]);
    $("#config_title_en").html(tileConfig["configTitleEn"]);
    $("#config_subtitle_nl").html(tileConfig["configSubTitleNl"]);
    $("#config_subtitle_fr").html(tileConfig["configSubTitleFr"]);
    $("#config_subtitle_en").html(tileConfig["configSubTitleEn"]);


    //Bottom Ctas

    function handleContentsBottomCtas(lang, Lng, i) {

        for (var j = 1; j <= i; j++) {
            var s1 = "#bottom_link_" + j + "_" + lang,
                o1 = "bottomLink" + j + "Text" + Lng,
                o2 = "bottomLink" + j + "Url" + Lng,
                icon = "bottomLink" + j + "IconClass";

            $(s1 + " i").addClass(tileConfig[icon]);
            $(s1 + " h5").html(tileConfig[o1]);
            $(s1).attr('href', tileConfig[o2]);
        }

    }


    // handle visibility Bottom Ctas

    function handleVisibilityBottomCtas(lang, Lng) {

        var s1 = "#bottom_link_1_" + lang,
            s2 = "#bottom_link_2_" + lang,
            s3 = "#bottom_link_3_" + lang,
            s4 = "#bottom_link_4_" + lang,
            o1 = "bottomLink1Text" + Lng,
            o2 = "bottomLink2Text" + Lng,
            o3 = "bottomLink3Text" + Lng,
            o4 = "bottomLink4Text" + Lng;

        if (tileConfig[o1] === "") {
            $(s1).hide();
        }

        if (tileConfig[o2] === "") {
            $(s2).hide();
        }
        if (tileConfig[o3] === "") {
            $(s3).hide();
        }
        if (tileConfig[o4] === "") {
            $(s4).hide();
        }

    }

    // Fill up the content
    handleContentsBottomCtas('nl', 'Nl', 4);
    handleContentsBottomCtas('fr', 'Fr', 4);
    handleContentsBottomCtas('en', 'En', 4);

    // Handle visibility of empty buttons
    handleVisibilityBottomCtas('nl', 'Nl');
    handleVisibilityBottomCtas('fr', 'Fr');
    handleVisibilityBottomCtas('en', 'En');





    //Popover Status
    showPopup = tileConfig.popoverStatus;

    //Popover Image Url
    var bgurl = 'url(' + tileConfig.popoverBackgroundImage + ')';
    $("#popover").css('background-image', bgurl);
    $("#popover").css('margin-top', tileConfig.popoverMarginTop);


    //Popover titles
    $("#popover_title_nl").html(tileConfig["popoverTitleNl"]);
    $("#popover_title_fr").html(tileConfig["popoverTitleFr"]);
    $("#popover_title_en").html(tileConfig["popoverTitleEn"]);
    $("#popover_subtitle_nl").html(tileConfig["popoverSubTitleNl"]);
    $("#popover_subtitle_fr").html(tileConfig["popoverSubTitleFr"]);
    $("#popover_subtitle_en").html(tileConfig["popoverSubTitleEn"]);

    //Popover CTA
    $("#popover_cta_nl").html(tileConfig["popoverLinkTextNl"] + ' <i class="proxicon-Arrow"></i>');
    $("#popover_cta_fr").html(tileConfig["popoverLinkTextFr"] + ' <i class="proxicon-Arrow"></i>');
    $("#popover_cta_en").html(tileConfig["popoverLinkTextEn"] + ' <i class="proxicon-Arrow"></i>');
    $("#popover_cta_nl").attr('href', tileConfig["popoverLinkUrlNl"]);
    $("#popover_cta_fr").attr('href', tileConfig["popoverLinkUrlFr"]);
    $("#popover_cta_en").attr('href', tileConfig["popoverLinkUrlEn"]);


   

    // handle storage variable, to keep track of closing status. If you renew the name, the popover shows again.
    storageVar = tileConfig["dataStorageName"]
    $("#popover").attr('data-storagename', storageVar);
    var cc = localStorage.getItem(storageVar);

    if (!cc) {
        $(".hero-banner").hide();
        $(".ctas").hide();
        $(".onboarding-popup").show();
        //gadgets.window.adjustHeight()
    }
    
     if (!showPopup) {
        $(".onboarding-popup").hide();
        $(".hero-banner").show();
        $(".ctas").show();
    }
    
    //scriptblock

    $("#script_block").html(tileConfig["scriptBlock"]);

    // Resize window
    app.resize();
    if (typeof gadgets !== 'undefined') {
        gadgets.window.adjustHeight()
    }
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE CONFIG IS RESOLVED
//************************************************************************
function onConfig(tileConfig, tileOptions) {
    // console.log('onConfig', tileConfig, tileOptions);
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE CONTAINER IS RESOLVED
//************************************************************************
function onContainer(container) {
    //console.log('onContainer', container);
    if (typeof gadgets !== 'undefined') {
        gadgets.window.adjustHeight()
    }
} // end function

//************************************************************************
//NOTE: CALLED AS SOON AS THE VIEWER IS RESOLVED
//************************************************************************
function onViewer(viewer) {
    // console.log('onViewer', viewer);
} // end function



/* global $, gadgets */

var resizetimeout;

function createCookie(name, value, days) {
    var expires = "";
    //    if (days) {
    //        var date = new Date();
    //        date.setTime(date.getTime() + (100 * 24 * 60 * 60 * 1000));
    //        expires = "; expires=" + date.toUTCString();
    //    }
    //    document.cookie = name + "=" + value + expires + "; path=/";

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else expires = "";

    document.cookie = name + "=" + value + "; expires=Thu, 18 Dec 3013 12:00:00 UTC; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    //return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function cookieExists(cname) {
    return (document.cookie.indexOf(cname + '=') == -1) ? false : true;
}


$(function () {

// see if localstorage is supported
//    if (typeof (Storage) !== "undefined") {
//        // Code for localStorage/sessionStorage.
//        //console.log('webstorage supported')
//    } else {
//        console.log('Sorry! No Web Storage support..');
//    }
   


    //change this value every time you create a new version
    var cookieName = "prx-autumn-2017-hero";
    // resize on load
    if (typeof gadgets !== 'undefined') {
        gadgets.window.adjustHeight()
    }

    // resize after all images are loaded
    $('img').on('load', function () {
        if (typeof gadgets !== 'undefined') {
            gadgets.window.adjustHeight()
        }
    })

    // resize if window is resized
    $(window).resize(function () {
        clearTimeout(resizetimeout)
        resizetimeout = setTimeout(function () {
            gadgets.window.adjustHeight()
        }, 100)
    })

    function handleVisibiltyElements() {

        $(".onboarding-popup").hide();
        $(".hero-banner").show();
        $(".ctas").show();
        gadgets.window.adjustHeight()

        //store user closing action in local storage
        localStorage.setItem(storageVar, true);
        
        //make sure to start playing the video 
        $('#config_video')[0].play();
    }

// close button popver
    $(".proximus-close").on('click', function (e) {
        e.preventDefault();
        handleVisibiltyElements();
        return false;
    })

//enable simple cta  in popover to close the popup as well
    $(".proximus-button").on('click', function (e) {
//        e.preventDefault();
//        var url= $(this).attr('href');
        handleVisibiltyElements();
//        window.open(url,'_blank');
//        window.location.assign(url);
    })


    $('.popup-show').on('click', function (e) {
        e.preventDefault();
        $(".hero-banner").hide();
        $(".ctas").hide();
        $(".onboarding-popup").show();
        gadgets.window.adjustHeight();

        //set user opening action in local storage
        localStorage.setItem(storageVar, false);
        return false;
    })




    // testing, comment for deployment:
    //createCookie(cookieName,"closed", 100);


    /* Add your JS here */
})