/* global $, gadgets */

var resizetimeout

function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}


$(function () {
  // resize on load
  if (typeof gadgets !== 'undefined') { gadgets.window.adjustHeight() }

  // resize after all images are loaded
  $('img').on('load', function () {
    if (typeof gadgets !== 'undefined') { gadgets.window.adjustHeight() }
  })

  // resize if window is resized
  $(window).resize(function () {
    clearTimeout(resizetimeout)
    resizetimeout = setTimeout(gadgets.window.adjustHeight, 100)
  })

  $(".proximus-close").on('click', function (e) {
    e.preventDefault();
    $(".onboarding-popup").hide();
    $(".hero-banner").show();
    gadgets.window.adjustHeight()
    createCookie("wappy_will_return","closed", 100);
    return false;
  })

  $('.popup-show').on('click', function (e) {
    e.preventDefault();
    $(".hero-banner").hide();
    $(".onboarding-popup").show();
    gadgets.window.adjustHeight();
    eraseCookie("wappy_will_return");
    return false;
  })

  // testing, comment for deployment:
  //createCookie("onboarding-1","closed", 100);

if (readCookie("wappy_will_return") != "closed" ) {
  $(".hero-banner").hide();
  $(".onboarding-popup").show();
  gadgets.window.adjustHeight()
}
  /* Add your JS here */
})


