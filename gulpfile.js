const assembler = require('fabricator-assemble');
const browserSync = require('browser-sync');
const csso = require('gulp-csso');
const del = require('del');
const gulp = require('gulp');
const gutil = require('gulp-util');
const gulpif = require('gulp-if');
const imagemin = require('gulp-imagemin');
const prefix = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const reload = browserSync.reload;
const runSequence = require('run-sequence').use(gulp);;
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const webpack = require('webpack');
var postcss = require('gulp-postcss');
var classPrfx = require('postcss-class-prefix');
var scopify = require('postcss-scopify');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify');
var sassdoc = require('sassdoc');

// configuration
const config = {
    dev: gutil.env.dev,
    styles: {
        browsers: 'last 4 versions',
        fabricator: {
            src: 'src/assets/fabricator/styles/fabricator.scss',
            dest: 'dist/assets/fabricator/styles',
            watch: 'src/assets/fabricator/styles/**/*.scss',
        },
        toolkit: {
            src: 'src/assets/toolkit/styles/**/*.scss',
            dest: 'dist/assets/toolkit/styles',
            watch: 'src/assets/toolkit/styles/**/*.scss',
        },
    },
    scripts: {
        fabricator: {
            src: './src/assets/fabricator/scripts/fabricator.js',
            dest: 'dist/assets/fabricator/scripts',
            watch: 'src/assets/fabricator/scripts/**/*',
        },
        toolkit: {
            src: './src/assets/toolkit/scripts/toolkit.js',
            dest: 'dist/assets/toolkit/scripts',
            watch: 'src/assets/toolkit/scripts/**/*',
        },
    },
    images: {
        toolkit: {
            src: ['src/assets/toolkit/images/**/*', 'src/favicon.ico'],
            dest: 'dist/assets/toolkit/images',
            watch: 'src/assets/toolkit/images/**/*',
        },
    },
    templates: {
        watch: 'src/**/*.{html,md,json,yml}',
    },
    dest: 'dist',
};


// clean
gulp.task('clean', del.bind(null, [config.dest]));


// styles
gulp.task('styles:fabricator', () => {
    gulp.src(config.styles.fabricator.src)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        //.pipe(prefix('last 2 version'))
        //.pipe(gulpif(!config.dev, csso()))
        .pipe(rename('f.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.styles.fabricator.dest))
        .pipe(gulpif(config.dev, reload({
            stream: true
        })));
});

gulp.task('styles:toolkit', () => {
    gulp.src(config.styles.toolkit.src)
        .pipe(gulpif(config.dev, sourcemaps.init()))
        .pipe(sass({
            includePaths: './node_modules',
        }).on('error', sass.logError))
        .pipe(prefix('last 1 version'))
        .pipe(gulpif(!config.dev, csso()))

        .pipe(gulpif(config.dev, sourcemaps.write()))
        .pipe(gulp.dest(config.styles.toolkit.dest))
        .pipe(gulpif(config.dev, reload({
            stream: true
        })));
});

gulp.task('styles', ['styles:fabricator', 'styles:toolkit']);


// scripts
const webpackConfig = require('./webpack.config')(config);

gulp.task('scripts', (done) => {
    webpack(webpackConfig, (err, stats) => {
        if (err) {
            gutil.log(gutil.colors.red(err()));
        }
        const result = stats.toJson();
        if (result.errors.length) {
            result.errors.forEach((error) => {
                gutil.log(gutil.colors.red(error));
            });
        }
        done();
    });
});


// images
gulp.task('images', ['favicon'], () => {
    return gulp.src(config.images.toolkit.src)
        .pipe(imagemin())
        .pipe(gulp.dest(config.images.toolkit.dest));
});

gulp.task('favicon', () => {
    return gulp.src('src/favicon.ico')
        .pipe(gulp.dest(config.dest));
});


// assembler
gulp.task('assembler', (done) => {
    assembler({
        logErrors: config.dev,
        dest: config.dest,
    });
    done();
});


// server
gulp.task('serve', () => {

    browserSync({
        server: {
            baseDir: config.dest,
        },
        notify: false,
        logPrefix: 'FABRICATOR',
    });

    gulp.task('assembler:watch', ['assembler'], browserSync.reload);
    gulp.watch(config.templates.watch, ['assembler:watch']);

    gulp.task('styles:watch', ['styles']);
    gulp.watch([config.styles.fabricator.watch, config.styles.toolkit.watch], ['styles:watch']);

    gulp.task('scripts:watch', ['scripts', 'copyscripts', 'concatscripts'], browserSync.reload);
    gulp.watch([config.scripts.fabricator.watch, config.scripts.toolkit.watch], ['scripts:watch']);

    gulp.task('images:watch', ['images'], browserSync.reload);
    gulp.watch(config.images.toolkit.watch, ['images:watch']);

    gulp.task('pcss:watch', ['pcss'], browserSync.reload);
    gulp.watch('dist/assets/toolkit/styles/toolkit.css', ['pcss:watch']);

    gulp.task('copy:watch', ['copy'], browserSync.reload);
    gulp.watch('dist/assets/toolkit/fonts/**/*', ['copy:watch']);

    gulp.task('concatcss:watch', ['concatcss'], browserSync.reload);
    gulp.watch('dist/assets/toolkit/styles/prx-toolkit.css', ['concatcss:watch']);

});


// default build task
gulp.task('default', ['clean'], () => {

    // define build tasks
    const tasks = [
    'styles',
    'scripts',
    'images',
    'assembler',
      'pcss',
      'copy',
      'copyscripts',
      'concatscripts',
      'concatcss',
  ];

    // run build
    runSequence(tasks, () => {
        if (config.dev) {
            gulp.start('serve');
        }
    });

});

//prefix all css and scope all css selectors
gulp.task('pcss', function () {
    return gulp.src('dist/assets/toolkit/styles/toolkit.css')
        .pipe(postcss([classPrfx('prx-', {
            ignore: ['active', 'scroll-to-fixed-fixed', 'foundation-mq', 'accordion', 'accordion-title', 'accordion-content', 'j-tile']
        })]))
        .pipe(postcss([scopify('.prx-wrapper')]))
        .pipe(rename("prx-toolkit.css"))
        .pipe(gulp.dest('dist/assets/toolkit/styles'));
});

// copy static fonts files
gulp.task('copy', function () {
    return gulp.src('src/assets/toolkit/fonts/**/*')
        .pipe(gulp.dest('dist/assets/toolkit/fonts/'));

});

//copy sassdoc proximus theme to node-modules
gulp.task('copysassdoctheme', function () {
    return gulp.src('src/assets/sassdoc-theme-proximus/**/*')
        .pipe(gulp.dest('node_modules/sassdoc-theme-proximus/'));
});

// copy vendor scripts files
gulp.task('copyscripts', function () {
    return gulp.src('src/assets/toolkit/scripts/vendor/**/*.js')
        .pipe(gulp.dest('dist/assets/toolkit/scripts/vendor/'));

});

// copy identity script file so we can load it seperately in the header
gulp.task('copyidentityscript', function () {

    return gulp.src('src/assets/toolkit/scripts/identitySwitch.js')
        .pipe(gulp.dest('dist/assets/toolkit/scripts/'));
});


//create one js file from all  
gulp.task('concatscripts', function () {
    return gulp.src([
        'src/assets/toolkit/scripts/vendor/foundation.js',
        //'src/assets/toolkit/scripts/vendor/what-input.js',
        'src/assets/toolkit/scripts/vendor/jquery-scrolltofixed-static.js',
        'src/assets/toolkit/scripts/jive-hideAskButton.js',
        'src/assets/toolkit/scripts/toolkit.js',
        'src/assets/toolkit/scripts/custom.js',
        'src/assets/toolkit/scripts/announcements.js',
        'src/assets/toolkit/scripts/scroll-top.js',
        'src/assets/toolkit/scripts/collapsible.js',
        'src/assets/toolkit/scripts/column-transform.js',
        'src/assets/toolkit/scripts/customToc.js',
        'src/assets/toolkit/scripts/bubble-help.js',
        'src/assets/toolkit/scripts/svg-url-generate.js',
        'src/assets/toolkit/scripts/snow.js',
        'src/assets/toolkit/scripts/iframe-helper.js',
        //'src/assets/toolkit/scripts/search-link-target-setter.js',
        'src/assets/toolkit/scripts/canvas-avatar.js',
        //'src/assets/toolkit/scripts/media-screen-user.js'
        
        //'src/assets/toolkit/scripts/events.js'

        //'src/assets/toolkit/scripts/search-demo.js'



    ])
        .pipe(concat('prx-all.js'))
        .pipe(minify())
        .pipe(gulp.dest('dist/assets/toolkit/scripts/'));
});

//create one css file from all  
gulp.task('concatcss', function () {
    return gulp.src(['dist/assets/toolkit/styles/prx-toolkit.css', 'dist/assets/toolkit/styles/jive-overwrites.css'])
        .pipe(concat('prx-all.css'))
        .pipe(gulpif(!config.dev, minify()))
        .pipe(gulp.dest('dist/assets/toolkit/styles/'));
});


//Create sass documentation for jive overwrites
gulp.task('sassdoc', function () {
    return gulp.src('src/assets/toolkit/styles/jive-overwrites.scss')
        .pipe(sassdoc({
            theme: "proximus",
            package: {
                title: 'Wapcusto <-> Jive'
            },
            dest: "./dist/sassdoc/"
        }))


});


gulp.task('build-toolkit', function (done) {
    runSequence(
        'styles',
        'scripts',
        'images',
        'assembler',
        'pcss',
        'copy',
        'copysassdoctheme',
        'copyscripts',
        'concatscripts',
        'copyidentityscript',
        'concatcss',
        'sassdoc',
        function () {
            gulp.start('serve');
            console.log('Serving now');
            done();
        });
});