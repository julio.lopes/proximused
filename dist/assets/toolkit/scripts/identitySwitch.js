//goal is to switch devopers identity, to able to have different dependencies based upun the selected identity
// Cookies
function createCookiePrx(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";

    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookiePrx(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookiePrx(name) {
    createCookiePrx(name, "", -1);
}



var currentIdentity,
    identitypath,
    csspath, jspath,
    //collect paths in script tag
    csspathO = document.getElementById('identitySwitch').getAttribute("data-csspath"),
    jspathO = document.getElementById('identitySwitch').getAttribute("data-jspath");
    baseUrl = document.getElementById('identitySwitch').getAttribute("data-baseUrl");


function changeIdentity(identity) {

    //addDevJs(jspath); 
    document.cookie = "identity=" + identity + "; expires=Thu, 18 Dec 3013 12:00:00 UTC; path=/";
    thepath = createDevPaths(identity);
    //console.log("read cookie identity : " + identity);
    setDevCss(csspath);
    location.reload();
}


function createDevPaths(identity) {
    // grab base path from data in html
    identypath = baseUrl + identity;
    csspath = identypath + csspathO;
    jspath = identypath + jspathO;
    //console.log(csspath);
}



// onchange select value
function identitySelectChange() {
    var select_id = document.getElementById("identitySelect");
    currentIdentity = select_id.options[select_id.selectedIndex].value;
    changeIdentity(currentIdentity);
}

// onchange select value
function setIdentitySelect(selectedvalue) {
    var select = document.getElementById("identitySelect");
    select.value = selectedvalue;
}


//Read Identity cookie or create a new one

if ((document.cookie.indexOf("identity=") < 0)) {
    //no cookie
    //console.log('no identity');
    createCookiePrx('identity', '', '');
} else {
    var id = readCookiePrx('identity'),
        thepath = createDevPaths(id);
    //console.log("read cookie identity : " + id);
    setDevCss(csspath);
    //console.log('i did it');


}

function setDevCss(path) {

    var cssId = 'prx-css'; // you could encode the css path itself to generate id..
    if (!document.getElementById(cssId)) {
        var head = document.getElementsByTagName('head')[0];
        var link = document.createElement('link');
        link.id = cssId;
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = path;
        link.media = 'all';
        head.appendChild(link);
    } else {
        document.getElementById(cssId).setAttribute('href', path);
    }
}

function setDevJs(id) {

    var jsId = 'prx-js';
    var prxScript = document.getElementById(jsId);
    createDevPaths(id);
    prxScript.setAttribute('src', jspath);

}

var select = document.createElement('select');

function addIdentityDropDown() {

    //var identities = ['', '/julio', '/peter'];

    var identities = JSON.parse(document.getElementById(("identitySwitch")).getAttribute("data-identityPaths"));

    // Use the Option constructor: args text, value, defaultSelected, selected
    var option = new Option('- Select Identity -', '', false, false);
    select.appendChild(option);

    identities.forEach(function (identity, index) {
        // Use createElement to add an option:
        option = document.createElement('option');
        option.value = identity;
        option.text = identity;
        select.appendChild(option);
    });

}

// append select to the dom when ready
function r(f) {
    /in/.test(document.readyState) ? setTimeout('r(' + f + ')', 9) : f()
}
// create identity select
r(function () {


    var selectdiv = document.createElement("div"),
        t = document.createTextNode(""),
        id = readCookiePrx('identity');

    // Create a wrapping div element
    selectdiv.setAttribute('id', "prx-select-wrap");
    selectdiv.setAttribute('style', "text-align:right;position:fixed; bottom:0;right:0;width:150px;z-index:100;");
    // Create a text node
    selectdiv.appendChild(t);
    // Add to the dom
    document.body.appendChild(selectdiv);



    //assign id and function name on change;
    select.setAttribute("id", "identitySelect");
    select.setAttribute("onchange", "identitySelectChange()");
    document.getElementById('prx-select-wrap').appendChild(select);
    setIdentitySelect(id);
    setDevJs(id);
})

addIdentityDropDown();